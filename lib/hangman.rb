#require "dictionary"

class Hangman
  attr_reader :guesser, :referee, :board, :count

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @count = 0
  end

  def play
    setup
    alive = true
    while @board.include?("_") && alive
      self.take_turn
      alive = !game_over?(@count)
    end
    draw_hangman(@count)
    puts "\nword: " + @board.each_char.reduce("") {|word, c| word + c + " "}
    puts (alive ? "guesser wins!" : "referee wins!")
    puts "game over"
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = "_" * length
  end

  def take_turn
    draw_hangman(@count)
    puts "\nword: " + @board.each_char.reduce("") {|word, c| word + c + " "}
    letter = @guesser.guess(@board)
    indices = @referee.check_guess(letter)
    self.update_board(indices, letter)
    guesser.handle_response(letter, indices)
    @count += 1 if indices.length == 0
  end

  def game_over?(count)
    count == 6
  end

  def update_board(indices, letter)
    indices.each do |i|
      @board[i] = letter
    end
  end

  def draw_hangman(wrong_guesses)
    body_parts = ["O", "|", "\\", "/", "/", "\\"]
    b = body_parts.map.with_index {|part, i| i < wrong_guesses ? part : " "}

    body = "  __ \n" +
            " |  |\n" +
            " #{b[0]}  |\n" +
            "#{b[2]}#{b[1]}#{b[3]} |\n" +
            " #{b[1]}  |\n" +
            "#{b[4]} #{b[5]} |\n" +
            "   _|_\n"
    puts body
  end

end

class HumanPlayer
  attr_accessor :dictionary, :secret_word, :secret_length, :guessed_letters

  def initialize(dictionary)
    @dictionary = dictionary
    @guessed_letters = []
  end

  def pick_secret_word
    puts "Enter the length of the word you are thinking of."
    length = 0
    while length < 1
      length = gets.chomp.to_i
    end
    length
  end

  def check_guess(letter)
    puts "Guesser guesses '#{letter}', does this appear in your word (y/n)?"
    does_appear = or?(gets.chomp.downcase, "yes", "y")
    if does_appear
      puts "Please enter the indexes at which it appears (separated by spaces)"
      res = gets.chomp
      indices = res.split.map(&:to_i).uniq
    else
      []
    end
  end

  def or?(comparee, *args)
    args.any? {|arg| arg == comparee}
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def guess(board)
    #show all previously guessed letters
    puts "previous guesses: #{@guessed_letters.reduce("") {|word, c| word + c + ", "}}"
    puts "make a guess"
    is_valid = false
    alpha = ("a".."z").to_a.join
    while !is_valid
      letter = gets.chomp.downcase
      is_valid = letter.length == 1 && alpha.include?(letter)
    end
    @guessed_letters.push(letter)
    letter
  end

  def handle_response(letter, occurences)
    if occurences.length > 0
      puts "#{letter} occured #{occurences.length} times."
    else
      "Sorry, #{letter} did not occur in the word"
    end
  end

end

class ComputerPlayer
  attr_accessor :dictionary, :secret_word, :secret_length, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
  end

  def pick_secret_word
    len = @dictionary.length
    @secret_word = @dictionary[rand(len)]
    @secret_word.length
  end

  def check_guess(letter)
    @secret_word.each_char.each_with_index.reduce([]) do |indicies, (ch, i)|
      ch == letter ? indicies + [i] : indicies
    end
  end

  def register_secret_length(length)
    @candidate_words = @candidate_words.select {|word| word.length <= length}
    @secret_length = length
  end

  def guess(board)
    hash = Hash.new(0)
    @candidate_words.each do |word|
      word.each_char {|ch| hash[ch] += 1}
    end
    alpha = ("a".."z").to_a
    remaining_letters = alpha.select {|letter| !board.include?(letter)}
    filtered_count = hash.select {|key, value| remaining_letters.include?(key)}
    if filtered_count.length == 0
      raise "Uh oh, looks like your secret word is not a word the computer knows!"
    end
    letter = filtered_count.max_by{|key, value| value}[0]
  end

  def handle_response(letter, occurences)
    if occurences.length > 0
      @candidate_words = @candidate_words.select do |word|
        occurences.all? {|i| word[i] == letter} &&
        word.count(letter) == occurences.length
      end
    else
      @candidate_words = @candidate_words.select {|word| !word.include?(letter)}
    end
  end

end

dictionary = File.readlines("./dictionary.txt").map{|word| word.chomp}

guesser = HumanPlayer.new(dictionary)
ref = ComputerPlayer.new(dictionary)
game = Hangman.new({:referee => ref, :guesser => guesser})
game.play
